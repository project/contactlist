## CONTENTS OF THIS FILE

* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* Maintainers


## INTRODUCTION

Contact List version 8.x-1.x allows users of a Drupal website to
manage their contact lists and store and use information on their
personal contacts. Contacts are implemented as D8 entities and
therefore have all the features

Out of the box it contains the following fields:

- name
- email address
- phone number
- group

Additional fields can be attached to the contact entity using the Entity Fields UI and can be customized as desired.

The module also provides functionality for quick, advanced and bulk importation of contacts from CSV files.

The module is made as flexible as possible to fit into any number of use cases.

## REQUIREMENTS

It requires the core modules **Telephone** and **User**.


## RECOMMENDED MODULES

* [Advanced Help][4]:  
  When this module is enabled, display of the project's `README.md`
  will be rendered when you visit
  `help/contactlist/README.md`.

## INSTALLATION

1. Install as you would normally install a contributed drupal
   module. See: [Installing modules][5] for further information.

2. Enable the **Contact List** module on the *Extend* page.  The
   database tables will be created automagically for you at this
   point.


## CONFIGURATION

Navigate to **Manage » Strucrure » Contact list » Settings** to
configure Contact List settings and fields.


# MAINTAINERS

**Mother May I** was created by [almaudoh][6] (Aniebiet Udoh) in 2016.  
The current maintainer for D9 is [gisle][7] (Gisle Hannemyr).



[1]: https://drupal.org/project/contactlist
[2]: https://drupal.org/project/issues/contactlist
[4]: https://www.drupal.org/project/advanced_help
[5]: https://www.drupal.org/docs/extending-drupal/installing-modules
[6]: https://www.drupal.org/u/almaudoh
[7]: https://www.drupal.org/u/gisle
